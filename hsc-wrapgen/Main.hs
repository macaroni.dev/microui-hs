module Main where

import Text.Megaparsec
import Control.Monad.Reader
import Data.Map (Map)
import Data.List.NonEmpty (NonEmpty)
import Data.Aeson qualified as Ae
import Data.Void
import Data.Text (Text)
import Data.Text.Lazy.Builder (Builder)
import Data.Text.Lazy.Builder qualified as Builder
import Data.Map qualified as Map
import GHC.Generics

type P = ParsecT Void Text (ReaderT Cfg IO)

data G = G
  { files :: Map FilePath Builder
  }

instance Semigroup G where
  G x <> G y = G $ Map.unionWith (<>) x y

instance Monoid G where
  mempty = G mempty

data Cfg = Cfg
  { inputFiles :: NonEmpty FilePath
  , outdir :: FilePath
  , lineMode :: LineMode
  , modulePrefix :: Maybe String
  }

data LineMode = Line'Lenient LineMode | Line'Numbers [Int] | Line'All
  deriving stock (Eq, Show, Generic)
  deriving anyclass (Ae.FromJSON, Ae.ToJSON)

main :: IO ()
main = pure ()

pmain :: P G
pmain = do
  pure mempty
