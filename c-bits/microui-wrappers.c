#include "microui.h"

// Context:

void mu_push_clip_rect_w(mu_Context *ctx, mu_Rect *rect) {
  mu_push_clip_rect(ctx, *rect);
}
void mu_get_clip_rect_w(mu_Context *ctx, mu_Rect *out) {
  *out = mu_get_clip_rect(ctx);
}
int mu_check_clip_w(mu_Context *ctx, mu_Rect *r) {
  return mu_check_clip(ctx, *r);
}

//// Context Getters/Setters:

void mu_set_text_width_w(mu_Context *ctx, int (*text_width)(mu_Font font, const char *str, int len)) {
  ctx->text_width = text_width;
}

void mu_set_text_height_w(mu_Context *ctx, int (*text_height)(mu_Font font)) {
  ctx->text_height = text_height;
}

void mu_set_draw_frame_w(mu_Context *ctx, void (*draw_frame)(mu_Context *ctx, mu_Rect rect, int colorid)) {
  ctx->draw_frame = draw_frame;
}

void mu_set_style_w(mu_Context *ctx, mu_Style *style) {
  *(ctx->style) = *style;
}

// Low-level:
void mu_set_clip_w(mu_Context *ctx, mu_Rect *rect) {
  mu_set_clip(ctx, *rect);
}

void mu_draw_rect_w(mu_Context *ctx, mu_Rect *rect, mu_Color *color) {
  mu_draw_rect(ctx, *rect, *color);
}

void mu_draw_box_w(mu_Context *ctx, mu_Rect *rect, mu_Color *color) {
  mu_draw_box(ctx, *rect, *color);
}

void mu_draw_text_w(mu_Context *ctx, mu_Font font, const char *str, int len, mu_Vec2 *pos, mu_Color *color) {
  mu_draw_text(ctx, font, str, len, *pos, *color);
}

void mu_draw_icon_w(mu_Context *ctx, int id, mu_Rect *rect, mu_Color *color) {
  mu_draw_icon(ctx, id, *rect, *color);
}

// Layout:

void mu_layout_set_next_w(mu_Context *ctx, mu_Rect *r, int relative) {
  mu_layout_set_next(ctx, *r, relative);
}

void mu_layout_next_w(mu_Context *ctx, mu_Rect *out) {
  *out = mu_layout_next(ctx);
}

// Control

void mu_draw_control_frame_w(mu_Context *ctx, mu_Id id, mu_Rect *rect, int colorid, int opt) {
  mu_draw_control_frame(ctx, id, *rect, colorid, opt);
}

void mu_draw_control_text_w(mu_Context *ctx, const char *str, mu_Rect *rect, int colorid, int opt) {
  mu_draw_control_text(ctx, str, *rect, colorid, opt);
}

int mu_mouse_over_w(mu_Context *ctx, mu_Rect *rect) {
  return mu_mouse_over(ctx, *rect);
}

void mu_update_control_w(mu_Context *ctx, mu_Id id, mu_Rect *rect, int opt) {
  mu_update_control(ctx, id, *rect, opt);
}

// Controls

//// Macros
int mu_button_w(mu_Context *ctx, const char *label) {
  return mu_button(ctx, label);
}

int mu_textbox_w(mu_Context *ctx, char *buf, int bufsz) {
  return mu_textbox(ctx, buf, bufsz);
}

int mu_slider_w(mu_Context *ctx, mu_Real *value, mu_Real low, mu_Real high) {
  return mu_slider(ctx, value, low, high);
}

int mu_number_w(mu_Context *ctx, mu_Real *value, mu_Real step) {
  return mu_number(ctx, value, step);
}

int mu_header_w(mu_Context *ctx, const char *label) {
  return mu_header(ctx, label);
}

int mu_begin_treenode_w(mu_Context *ctx, const char *label) {
  return mu_begin_treenode(ctx, label);
}

int mu_begin_window_w(mu_Context *ctx, const char *title, mu_Rect *rect) {
  return mu_begin_window(ctx, title, *rect);
}

void mu_begin_panel_w(mu_Context *ctx, const char *name) {
  return mu_begin_panel(ctx, name);
}

//// Functions
int mu_textbox_raw_w(mu_Context *ctx, char *buf, int bufsz, mu_Id id, mu_Rect *r, int opt) {
  return mu_textbox_raw(ctx, buf, bufsz, id, *r, opt);
}

int mu_begin_window_ex_w(mu_Context *ctx, const char *title, mu_Rect *rect, int opt) {
  return mu_begin_window_ex(ctx, title, *rect, opt);
}
