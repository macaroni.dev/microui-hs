{ crossMingwW64 ? false }:
let pkgs = import ./nix/pkgs.nix;
in (import ./default.nix).shellFor {
  tools = {
    cabal = "latest";
    hsc2hs = "latest";
  };

  withHoogle = false;

  buildInputs = [ ];
}
