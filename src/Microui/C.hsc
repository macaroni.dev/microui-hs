module Microui.C where

import Prelude hiding (Real)
import Data.Word
import Data.Int
import Foreign.Ptr
import Foreign.C.String
import Data.Void
import Data.Vector.Sized as V

import Microui.C.Types

-- Context

foreign import ccall "microui.h mu_init" init
  :: Ptr Context
  -> IO ()

foreign import ccall "microui.h mu_begin" begin
  :: Ptr Context
  -> IO ()

foreign import ccall "microui.h mu_end" end
  :: Ptr Context
  -> IO ()

foreign import ccall "microui.h mu_set_focus" setFocus
  :: Ptr Context
  -> Id
  -> IO ()

foreign import ccall "microui.h mu_get_id" getId
  :: Ptr Context
  -> Ptr a
  -> #{type int} -- ^ size
  -> IO Id

foreign import ccall "microui.h mu_push_id" pushId
  :: Ptr Context
  -> Ptr a
  -> #{type int} -- ^ size
  -> IO ()

foreign import ccall "microui.h mu_pop_id" popId
  :: Ptr Context
  -> IO ()

foreign import ccall "mu_push_clip_rect_w" pushClipRect
  :: Ptr Context
  -> Ptr Rect
  -> IO ()

foreign import ccall "microui.h mu_pop_clip_rect" popClipRect
  :: Ptr Context
  -> IO ()

foreign import ccall "mu_get_clip_rect_w" getClipRect
  :: Ptr Context
  -> Ptr Rect -- ^ out
  -> IO ()

foreign import ccall "mu_check_clip_w" checkClip
  :: Ptr Context
  -> Ptr Rect
  -> IO #{type int}

foreign import ccall "microui.h mu_get_current_container" getCurrentContainer
  :: Ptr Context
  -> IO (Ptr Container)

foreign import ccall "microui.h mu_get_container" getContainer
  :: Ptr Context
  -> CString -- ^ name
  -> IO (Ptr Container)

foreign import ccall "microui.h mu_bring_to_front" bringToFront
  :: Ptr Context
  -> Ptr Container
  -> IO ()

foreign import ccall "mu_set_text_width_w" setTextWidth
  :: Ptr Context
  -> FunPtr (Font -> CString -> #{type int} -> #{type int})
  -> IO ()

foreign import ccall "mu_set_text_height_w" setTextHeight
  :: Ptr Context
  -> FunPtr (Font -> #{type int})
  -> IO ()

-- TODO: draw_frame setter

foreign import ccall "mu_set_style_w" setStyle
  :: Ptr Context
  -> Ptr Style
  -> IO ()

-- Pool

foreign import ccall "microui.h mu_pool_init" poolInit
  :: Ptr Context
  -> Ptr PoolItem
  -> #{type int} -- len
  -> Id
  -> IO #{type int}

foreign import ccall "microui.h mu_pool_get" poolGet
  :: Ptr Context
  -> Ptr PoolItem
  -> #{type int} -- len
  -> Id
  -> IO #{type int}

foreign import ccall "microui.h mu_pool_update" poolUpdate
  :: Ptr Context
  -> Ptr PoolItem
  -> #{type int} -- idx
  -> IO ()

-- Input
foreign import ccall "microui.h mu_input_mousemove" inputMouseMove
  :: Ptr Context
  -> #{type int} -- ^ x
  -> #{type int} -- ^ y
  -> IO ()

foreign import ccall "microui.h mu_input_mousedown" inputMouseDown
  :: Ptr Context
  -> #{type int} -- ^ x
  -> #{type int} -- ^ y
  -> MouseButton
  -> IO ()

foreign import ccall "microui.h mu_input_mouseup" inputMouseUp
  :: Ptr Context
  -> #{type int} -- ^ x
  -> #{type int} -- ^ y
  -> MouseButton
  -> IO ()

foreign import ccall "microui.h mu_input_scroll" inputScroll
  :: Ptr Context
  -> #{type int} -- ^ x
  -> #{type int} -- ^ y
  -> IO ()

foreign import ccall "microui.h mu_input_keydown" inputKeyDown
  :: Ptr Context
  -> #{type int} -- ^ key
  -> IO ()

foreign import ccall "microui.h mu_input_keyup" inputKeyUp
  :: Ptr Context
  -> #{type int} -- ^ key
  -> IO ()

foreign import ccall "microui.h mu_input_text" inputText
  :: Ptr Context
  -> CString
  -> IO ()


-- Commands:
foreign import ccall "microui.h mu_push_command" pushCommand
  :: Ptr Context
  -> #{type int} -- ^ type
  -> #{type int} -- ^ size
  -> Ptr Command

foreign import ccall "microui.h mu_next_command" nextCommand
  :: Ptr Context
  -> Ptr (Ptr Command)
  -> IO #{type int}

foreign import ccall "mu_set_clip_w" setClip
  :: Ptr Context
  -> Ptr Rect
  -> IO ()

foreign import ccall "mu_draw_rect_w" drawRect
  :: Ptr Context
  -> Ptr Rect
  -> Ptr Color
  -> IO ()

foreign import ccall "mu_draw_box_w" drawBox
  :: Ptr Context
  -> Ptr Rect
  -> Ptr Color
  -> IO ()

foreign import ccall "mu_draw_text_w" drawText
  :: Ptr Context
  -> Font
  -> CString
  -> #{type int} -- ^ len
  -> Ptr Vec2
  -> Ptr Color
  -> IO ()

foreign import ccall "mu_draw_icon_w" drawIcon
  :: Ptr Context
  -> Icon
  -> Ptr Rect
  -> Ptr Color
  -> IO ()

-- Layout
foreign import ccall "microui.h mu_layout_row" layoutRow
  :: Ptr Context
  -> #{type int} -- ^ items
  -> Ptr #{type int} -- ^ widths
  -> #{type int} -- ^ height
  -> IO ()

foreign import ccall "microui.h mu_layout_width" layoutWidth
  :: Ptr Context
  -> #{type int} -- ^ width
  -> IO ()

foreign import ccall "microui.h mu_layout_height" layoutHeight
  :: Ptr Context
  -> #{type int} -- ^ width
  -> IO ()

foreign import ccall "microui.h mu_layout_begin_column" layoutBeginColumn
  :: Ptr Context
  -> IO ()

foreign import ccall "microui.h mu_layout_end_column" layoutEndColumn
  :: Ptr Context
  -> IO ()

foreign import ccall "mu_layout_set_next_w" layoutSetNext
  :: Ptr Context
  -> Ptr Rect
  -> #{type int} -- ^ relative
  -> IO ()

foreign import ccall "mu_layout_next_w" layoutNext
  :: Ptr Context
  -> Ptr Rect -- ^ out
  -> IO ()

-- Control

foreign import ccall "mu_draw_control_frame_w" drawControlFrame
  :: Ptr Context
  -> Id
  -> Ptr Rect
  -> #{type int} -- ^ colorid
  -> Opt
  -> IO ()

foreign import ccall "mu_draw_control_text_w" drawControlText
  :: Ptr Context
  -> CString
  -> Ptr Rect
  -> #{type int} -- ^ colorid
  -> Opt
  -> IO ()

foreign import ccall "mu_mouse_over_w" mouseOver
  :: Ptr Context
  -> Ptr Rect
  -> IO #{type int}

foreign import ccall "mu_update_control_w" updateControl
  :: Ptr Context
  -> Id
  -> Ptr Rect
  -> Opt
  -> IO ()

-- Controls

foreign import ccall "mu_button_w" button
  :: Ptr Context
  -> CString
  -> IO #{type int}

foreign import ccall "mu_textbox_w" textbox
  :: Ptr Context
  -> CString
  -> #{type int} -- bufsz
  -> IO #{type int}

foreign import ccall "mu_slider_w" slider
  :: Ptr Context
  -> Ptr Real -- Value
  -> Real -- lo
  -> Real -- hi
  -> IO #{type int}

foreign import ccall "mu_number_w" number
  :: Ptr Context
  -> Ptr Real -- value
  -> Real -- step
  -> IO #{type int}

foreign import ccall "mu_header_w" header
  :: Ptr Context
  -> CString
  -> IO #{type int}

foreign import ccall "mu_begin_treenode_w" beginTreenode
  :: Ptr Context
  -> CString -- label
  -> IO #{type int}

foreign import ccall "mu_begin_window_w" beginWindow
  :: Ptr Context
  -> CString -- title
  -> Ptr Rect
  -> IO #{type int}

foreign import ccall "mu_begin_panel_w" beginPanel
  :: Ptr Context
  -> CString -- name
  -> IO ()

foreign import ccall "microui.h mu_text" text
  :: Ptr Context
  -> CString
  -> IO ()

foreign import ccall "microui.h mu_label" label
  :: Ptr Context
  -> CString
  -> IO ()

foreign import ccall "microui.h mu_button_ex" buttonEx
  :: Ptr Context
  -> CString
  -> Icon
  -> Opt
  -> IO #{type int}

foreign import ccall "microui.h mu_checkbox" checkbox
  :: Ptr Context
  -> CString
  -> Ptr #{type int} -- state
  -> IO #{type int}

foreign import ccall "mu_textbox_raw_w" textboxRaw
  :: Ptr Context
  -> CString -- buf
  -> #{type int} -- bufsz
  -> Id
  -> Ptr Rect
  -> Opt
  -> IO #{type int}

foreign import ccall "microui.h mu_textbox_ex" textboxEx
  :: Ptr Context
  -> CString -- buf
  -> #{type int} -- bufsz
  -> Opt
  -> IO #{type int}

foreign import ccall "microui.h mu_slider_ex" sliderEx
  :: Ptr Context
  -> Ptr Real -- value
  -> Real -- low
  -> Real -- high
  -> Real -- step
  -> CString -- fmt
  -> Opt
  -> IO #{type int}

foreign import ccall "microui.h mu_number_ex" numberEx
  :: Ptr Context
  -> Ptr Real -- value
  -> Real -- step
  -> CString -- fmt
  -> Opt
  -> IO #{type int}

foreign import ccall "microui.h mu_header_ex" headerEx
  :: Ptr Context
  -> CString -- label
  -> Opt
  -> IO #{type int}

foreign import ccall "microui.h mu_begin_treenode_ex" beginTreenodeEx
  :: Ptr Context
  -> CString -- label
  -> Opt
  -> IO #{type int}

foreign import ccall "microui.h mu_end_treenode" endTreenode
  :: Ptr Context
  -> IO ()

foreign import ccall "mu_begin_window_ex_w" beginWindowEx
  :: Ptr Context
  -> CString -- title
  -> Ptr Rect
  -> Opt
  -> IO #{type int}

foreign import ccall "microui.h mu_end_window" endWindow
  :: Ptr Context
  -> IO ()

foreign import ccall "microui.h mu_open_popup" openPopup
  :: Ptr Context
  -> CString
  -> IO ()

foreign import ccall "microui.h mu_begin_popup" beginPopup
  :: Ptr Context
  -> CString
  -> IO #{type int}

foreign import ccall "microui.h mu_end_popup" endPopup
  :: Ptr Context
  -> IO ()

foreign import ccall "microui.h mu_begin_panel_ex" beginPanelEx
  :: Ptr Context
  -> CString -- name
  -> Opt
  -> IO ()


foreign import ccall "microui.h mu_end_panel" endPanel
  :: Ptr Context
  -> IO ()
