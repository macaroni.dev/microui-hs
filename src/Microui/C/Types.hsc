{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE NoFieldSelectors #-}
{-# LANGUAGE LambdaCase #-}

module Microui.C.Types where

import Data.Word
import Data.Int
import Foreign.Storable
import Foreign.Ptr
import Foreign.C.String
import Data.Void
import Foreign.Marshal.Array (advancePtr)
import Data.Vector.Sized as V

#include <microui.h>
  
data Clip = Clip'Part | Clip'All deriving (Show, Eq)

newtype Id = Id #{type mu_Id}
  deriving stock (Show, Eq, Ord)
  deriving newtype (Storable)

newtype Real = Real #{type mu_Real}
  deriving stock (Show, Eq, Ord)
  deriving newtype (Storable)

newtype Font = Font (Ptr Void)
  deriving stock (Show, Eq, Ord)
  deriving newtype (Storable)

data Vec2 = Vec2
  { x :: #{type int}
  , y :: #{type int}
  } deriving (Show, Eq, Ord)

instance Storable Vec2 where
  alignment _ = #{alignment mu_Vec2}
  sizeOf _ = #{size mu_Vec2}

  poke ptr Vec2{..} = do
    #{poke mu_Vec2, x} ptr x
    #{poke mu_Vec2, y} ptr y

  peek ptr = do
    x <- #{peek mu_Vec2, x} ptr
    y <- #{peek mu_Vec2, y} ptr
    pure Vec2{..}

data Rect = Rect
  { x :: #{type int}
  , y :: #{type int}
  , w :: #{type int}
  , h :: #{type int}
  } deriving (Show, Eq, Ord)

instance Storable Rect where
  alignment _ = #{alignment mu_Rect}
  sizeOf _ = #{size mu_Rect}

  poke ptr Rect{..} = do
    #{poke mu_Rect, x} ptr x
    #{poke mu_Rect, y} ptr y
    #{poke mu_Rect, w} ptr w
    #{poke mu_Rect, h} ptr h

  peek ptr = do
    x <- #{peek mu_Rect, x} ptr
    y <- #{peek mu_Rect, y} ptr
    w <- #{peek mu_Rect, w} ptr
    h <- #{peek mu_Rect, h} ptr
    pure Rect{..}

data Color = Color
  { r :: #{type unsigned char}
  , g :: #{type unsigned char}
  , b :: #{type unsigned char}
  , a :: #{type unsigned char}
  } deriving (Show, Eq, Ord)

instance Storable Color where
  alignment _ = #{alignment mu_Color}
  sizeOf _ = #{size mu_Color}

  poke ptr Color{..} = do
    #{poke mu_Color, r} ptr r
    #{poke mu_Color, g} ptr g
    #{poke mu_Color, b} ptr b
    #{poke mu_Color, a} ptr a

  peek ptr = do
    r <- #{peek mu_Color, r} ptr
    g <- #{peek mu_Color, g} ptr
    b <- #{peek mu_Color, b} ptr
    a <- #{peek mu_Color, a} ptr
    pure Color{..}

data PoolItem = PoolItem
  { id_ :: Id
  , lastUpdate :: #{type int}
  } deriving (Show, Eq, Ord)

data BaseCommandC = BaseCommandC
  { type_ :: #{type int}
  , size_ :: #{type int}
  } deriving (Show, Eq, Ord)

instance Storable BaseCommandC where
  alignment _ = #{alignment mu_BaseCommand}
  sizeOf _ = #{size mu_BaseCommand}

  poke ptr BaseCommandC{..} = do
    #{poke mu_BaseCommand, type} ptr type_
    #{poke mu_BaseCommand, size} ptr size_

  peek ptr = do
    type_ <- #{peek mu_BaseCommand, type} ptr
    size_ <- #{peek mu_BaseCommand, size} ptr
    pure BaseCommandC{..}

data JumpCommandC = JumpCommandC
  { base :: BaseCommandC
  , dst :: Ptr Void
  } deriving (Show, Eq, Ord)

instance Storable JumpCommandC where
  alignment _ = #{alignment mu_JumpCommand}
  sizeOf _ = #{size mu_JumpCommand}

  poke ptr JumpCommandC{..} = do
    #{poke mu_JumpCommand, base} ptr base
    #{poke mu_JumpCommand, dst} ptr dst

  peek ptr = do
    base <- #{peek mu_JumpCommand, base} ptr
    dst <- #{peek mu_JumpCommand, dst} ptr
    pure JumpCommandC{..}

data ClipCommandC = ClipCommandC
  { base :: BaseCommandC
  , rect :: Rect
  } deriving (Show, Eq, Ord)

instance Storable ClipCommandC where
  alignment _ = #{alignment mu_ClipCommand}
  sizeOf _ = #{size mu_ClipCommand}

  poke ptr ClipCommandC{..} = do
    #{poke mu_ClipCommand, base} ptr base
    #{poke mu_ClipCommand, rect} ptr rect

  peek ptr = do
    base <- #{peek mu_ClipCommand, base} ptr
    rect <- #{peek mu_ClipCommand, rect} ptr
    pure ClipCommandC{..}

data RectCommandC = RectCommandC
  { base :: BaseCommandC
  , rect :: Rect
  , color :: Color
  } deriving (Show, Eq, Ord)

instance Storable RectCommandC where
  alignment _ = #{alignment mu_RectCommand}
  sizeOf _ = #{size mu_RectCommand}

  poke ptr RectCommandC{..} = do
    #{poke mu_RectCommand, base} ptr base
    #{poke mu_RectCommand, rect} ptr rect
    #{poke mu_RectCommand, color} ptr color

  peek ptr = do
    base <- #{peek mu_RectCommand, base} ptr
    rect <- #{peek mu_RectCommand, rect} ptr
    color <- #{peek mu_RectCommand, color} ptr
    pure RectCommandC{..}

data TextCommandC = TextCommandC
  { base :: BaseCommandC
  , font :: Font
  , pos :: Vec2
  , color :: Color
  , str :: String
  } deriving (Show, Eq, Ord)

instance Storable TextCommandC where
  alignment _ = #{alignment mu_TextCommand}
  sizeOf _ = #{size mu_TextCommand}

  poke ptr TextCommandC{..} = do
    #{poke mu_TextCommand, base} ptr base
    #{poke mu_TextCommand, font} ptr font
    #{poke mu_TextCommand, pos} ptr pos
    #{poke mu_TextCommand, color} ptr color
    withCString str $ #{poke mu_TextCommand, str} ptr

  peek ptr = do
    base <- #{peek mu_TextCommand, base} ptr
    font <- #{peek mu_TextCommand, font} ptr
    pos <- #{peek mu_TextCommand, pos} ptr
    color <- #{peek mu_TextCommand, color} ptr
    str <- #{peek mu_TextCommand, str} ptr >>= peekCString
    pure TextCommandC{..}

data IconCommandC = IconCommandC
  { base :: BaseCommandC
  , rect :: Rect
  , icon :: Icon
  , color :: Color
  } deriving (Show, Eq, Ord)

instance Storable IconCommandC where
  alignment _ = #{alignment mu_IconCommand}
  sizeOf _ = #{size mu_IconCommand}

  poke ptr IconCommandC{..} = do
    #{poke mu_IconCommand, base} ptr base
    #{poke mu_IconCommand, rect} ptr rect
    #{poke mu_IconCommand, id} ptr icon
    #{poke mu_IconCommand, color} ptr color

  peek ptr = do
    base <- #{peek mu_IconCommand, base} ptr
    rect <- #{peek mu_IconCommand, rect} ptr
    icon <- #{peek mu_IconCommand, id} ptr
    color <- #{peek mu_IconCommand, color} ptr
    pure IconCommandC{..}

data Command =
    ClipCommand{ rect :: Rect }
  | RectCommand{ rect :: Rect, color :: Color }
  | TextCommand{ font :: Font, pos :: Vec2, color :: Color, str :: String }
  | IconCommand{ rect :: Rect, icon :: Icon, color :: Color }
  deriving (Show, Eq, Ord)

instance Storable Command where
  alignment _ = #{alignment mu_Command}
  sizeOf _ = #{size mu_Command}
  
  poke ptr = \case
    ClipCommand{..} ->
      poke (castPtr ptr) ClipCommandC
      { base = BaseCommandC
        { type_ = #{const MU_COMMAND_CLIP}
        , size_ = fromIntegral $ sizeOf (undefined :: ClipCommandC) -- ?
        }
      , ..
      }
    RectCommand{..} ->
      poke (castPtr ptr) RectCommandC
      { base = BaseCommandC
        { type_ = #{const MU_COMMAND_RECT}
        , size_ = fromIntegral $ sizeOf (undefined :: RectCommandC) -- ?
        }
      , ..
      }
    TextCommand{..} ->
      poke (castPtr ptr) TextCommandC
      { base = BaseCommandC
        { type_ = #{const MU_COMMAND_TEXT}
        , size_ = fromIntegral $ sizeOf (undefined :: TextCommandC) -- ?
        }
      , ..
      }
    IconCommand{..} ->
      poke (castPtr ptr) IconCommandC
      { base = BaseCommandC
        { type_ = #{const MU_COMMAND_ICON}
        , size_ = fromIntegral $ sizeOf (undefined :: IconCommandC) -- ?
        }
      , ..
      }

  peek ptr = do
    t :: #{type int} <- #{peek mu_Command, type} ptr
    if | t == #{const MU_COMMAND_CLIP} -> do
           ClipCommandC{..} <- peek (castPtr ptr)
           pure ClipCommand{..}
       | t == #{const MU_COMMAND_RECT} -> do
           RectCommandC{..} <- peek (castPtr ptr)
           pure RectCommand{..}
       | t == #{const MU_COMMAND_TEXT} -> do
           TextCommandC{..} <- peek (castPtr ptr)
           pure TextCommand{..}
       | t == #{const MU_COMMAND_ICON} -> do
           IconCommandC{..} <- peek (castPtr ptr)
           pure IconCommand{..}
       | otherwise -> error $ unwords ["UNKNOWN COMMAND TYPE:", show t]

-- Internal?
data Layout = Layout
  { body :: Rect
  , next :: Rect
  , position :: Vec2
  , size_ :: Vec2
  , max_ :: Vec2
  , widths :: Vector #{const MU_MAX_WIDTHS} #{type int}
  , items :: #{type int}
  , itemIndex :: #{type int}
  , nextRow :: #{type int}
  , nextType :: #{type int}
  , indent :: #{type int}
  } deriving (Show, Eq, Ord)

-- Internal?
data Container = Container
  { head_ :: Command
  , tail_ :: Command
  , rect :: Rect
  , body :: Rect
  , contentSize :: Vec2
  , scroll :: Vec2
  , zindex :: #{type int}
  , open :: #{type int}
  } deriving (Show, Eq, Ord)


data StyleColors = StyleColors
  { text :: Color
  , border :: Color
  , windowBG :: Color
  , titleBG :: Color
  , titleText :: Color
  , panelBG :: Color
  , button :: Color
  , buttonHover :: Color
  , buttonFocus :: Color
  , base :: Color
  , baseHover :: Color
  , baseFocus :: Color
  , scrollBase :: Color
  , scrollThumb :: Color
  }

instance Storable StyleColors where
  alignment _ = #{alignment mu_Color}
  sizeOf _ = #{size mu_Color} * #{const MU_COLOR_MAX}

  poke ptr StyleColors{..} = do
    let p off (c :: Color) = poke (advancePtr (castPtr ptr) off) c
    p #{const MU_COLOR_TEXT} text
    p #{const MU_COLOR_BORDER} border
    p #{const MU_COLOR_WINDOWBG} windowBG
    p #{const MU_COLOR_TITLEBG} titleBG
    p #{const MU_COLOR_TITLETEXT} titleText
    p #{const MU_COLOR_PANELBG} panelBG
    p #{const MU_COLOR_BUTTON} button
    p #{const MU_COLOR_BUTTONHOVER} buttonHover
    p #{const MU_COLOR_BUTTONFOCUS} buttonFocus
    p #{const MU_COLOR_BASE} base
    p #{const MU_COLOR_BASEHOVER} baseHover
    p #{const MU_COLOR_BASEFOCUS} baseFocus
    p #{const MU_COLOR_SCROLLBASE} scrollBase
    p #{const MU_COLOR_SCROLLTHUMB} scrollThumb

  peek ptr = do
    let p off = peek (advancePtr (castPtr ptr) off) :: IO Color
    text <- p #{const MU_COLOR_TEXT}
    border <- p #{const MU_COLOR_BORDER}
    windowBG <- p #{const MU_COLOR_WINDOWBG}
    titleBG <- p #{const MU_COLOR_TITLEBG}
    titleText <- p #{const MU_COLOR_TITLETEXT}
    panelBG <- p #{const MU_COLOR_PANELBG}
    button <- p #{const MU_COLOR_BUTTON}
    buttonHover <- p #{const MU_COLOR_BUTTONHOVER}
    buttonFocus <- p #{const MU_COLOR_BUTTONFOCUS}
    base <- p #{const MU_COLOR_BASE}
    baseHover <- p #{const MU_COLOR_BASEHOVER}
    baseFocus <- p #{const MU_COLOR_BASEFOCUS}
    scrollBase <- p #{const MU_COLOR_SCROLLBASE}
    scrollThumb <- p #{const MU_COLOR_SCROLLTHUMB}
    pure StyleColors{..}

newtype Icon = Icon #{type int}
  deriving stock (Show, Eq, Ord)
  deriving newtype (Storable)

iconClose :: Icon
iconClose = Icon #{const MU_ICON_CLOSE}

iconCheck :: Icon
iconCheck = Icon #{const MU_ICON_CHECK}

iconCollapsed :: Icon
iconCollapsed = Icon #{const MU_ICON_COLLAPSED}

iconExpanded :: Icon
iconExpanded = Icon #{const MU_ICON_EXPANDED}

data Style = Style
  { font :: Font
  , size_ :: Vec2
  , padding :: #{type int}
  , spacing :: #{type int}
  , indent :: #{type int}
  , titleHeight :: #{type int}
  , scrollbarSize :: #{type int}
  , thumbSize :: #{type int}
  , colors :: StyleColors
  }

instance Storable Style where
  alignment _ = #{alignment mu_Style}
  sizeOf _ = #{size mu_Style}

  poke ptr Style{..} = do
    #{poke mu_Style, font} ptr font
    #{poke mu_Style, size} ptr size_
    #{poke mu_Style, padding} ptr padding
    #{poke mu_Style, spacing} ptr spacing
    #{poke mu_Style, indent} ptr indent
    #{poke mu_Style, title_height} ptr titleHeight
    #{poke mu_Style, scrollbar_size} ptr scrollbarSize
    #{poke mu_Style, thumb_size} ptr thumbSize
    #{poke mu_Style, colors} ptr colors

  peek ptr = do
    font <- #{peek mu_Style, font} ptr
    size_ <- #{peek mu_Style, size} ptr
    padding <- #{peek mu_Style, padding} ptr
    spacing <- #{peek mu_Style, spacing} ptr
    indent <- #{peek mu_Style, indent} ptr
    titleHeight <- #{peek mu_Style, title_height} ptr
    scrollbarSize <- #{peek mu_Style, scrollbar_size} ptr
    thumbSize <- #{peek mu_Style, thumb_size} ptr
    colors <- #{peek mu_Style, colors} ptr
    pure Style{..}


data Context

newtype MouseButton = MouseButton #{type int} deriving (Show, Eq, Ord)

mouseLeft :: MouseButton
mouseLeft = MouseButton #{const MU_MOUSE_LEFT}

mouseRight :: MouseButton
mouseRight = MouseButton #{const MU_MOUSE_RIGHT}

mouseMiddle :: MouseButton
mouseMiddle = MouseButton #{const MU_MOUSE_MIDDLE}

newtype Opt = Opt #{type int} deriving (Show, Eq, Ord)

optAlignCenter :: Opt
optAlignCenter = Opt #{const MU_OPT_ALIGNCENTER}

optAlignRight :: Opt
optAlignRight = Opt #{const MU_OPT_ALIGNRIGHT}

optNoInteract :: Opt
optNoInteract = Opt #{const MU_OPT_NOINTERACT}

optNoFrame :: Opt
optNoFrame = Opt #{const MU_OPT_NOFRAME}

optNoResize :: Opt
optNoResize = Opt #{const MU_OPT_NORESIZE}

optNoScroll :: Opt
optNoScroll = Opt #{const MU_OPT_NOSCROLL}

optNoClose :: Opt
optNoClose = Opt #{const MU_OPT_NOCLOSE}

optNoTitle :: Opt
optNoTitle = Opt #{const MU_OPT_NOTITLE}

optHoldFocus :: Opt
optHoldFocus = Opt #{const MU_OPT_HOLDFOCUS}

optAutoSize :: Opt
optAutoSize = Opt #{const MU_OPT_AUTOSIZE}

optPopup :: Opt
optPopup = Opt #{const MU_OPT_POPUP}

optClosed :: Opt
optClosed = Opt #{const MU_OPT_CLOSED}

optExpanded :: Opt
optExpanded = Opt #{const MU_OPT_EXPANDED}
