module Main where

import Graphics.Gloss
import Graphics.Gloss.Interface.IO.Game

import Microui.C
import Microui.C.Types

type W = ()
main :: IO ()
main = do
  playIO
    FullScreen
    black
    60
    ()
    draw
    handleEvents
    step

handleEvents :: Event -> W -> IO W
handleEvents _ w = pure w

step :: Float -> W -> IO W
step _ w = pure w

draw :: W -> IO Picture
draw _ = pure mempty
